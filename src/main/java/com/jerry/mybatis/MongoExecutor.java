package com.jerry.mybatis;

import com.jerry.mongo.converter.QueryConverter;
import com.mongodb.Function;
import com.mongodb.client.MongoDatabase;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.cache.CacheKey;
import org.apache.ibatis.executor.Executor;
import org.apache.ibatis.executor.statement.StatementHandler;
import org.apache.ibatis.mapping.*;
import org.apache.ibatis.plugin.*;
import org.apache.ibatis.reflection.MetaObject;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.ResultHandler;
import org.apache.ibatis.session.RowBounds;
import org.apache.ibatis.type.TypeHandlerRegistry;

import java.sql.Connection;
import java.text.DateFormat;
import java.util.*;
import java.util.regex.Matcher;

/**
 * @author jerry
 * mybatis拦截器处理mongo 的sql
 */

@Slf4j
@Intercepts(
        {@Signature(
                type = StatementHandler.class,
                method = "prepare",
                args = {Connection.class, Integer.class}
        ), @Signature(
                type = StatementHandler.class,
                method = "getBoundSql",
                args = {}
        ), @Signature(
                type = Executor.class,
                method = "update",
                args = {MappedStatement.class, Object.class}
        ), @Signature(
                type = Executor.class,
                method = "query",
                args = {MappedStatement.class, Object.class, RowBounds.class, ResultHandler.class}
        ), @Signature(
                type = Executor.class,
                method = "query",
                args = {MappedStatement.class, Object.class, RowBounds.class, ResultHandler.class, CacheKey.class, BoundSql.class}
        )}
)
public class MongoExecutor implements Interceptor {

    private MongoDatabase mongoDatabase;

    private Set<String> mongoCollectionNames;

    private TableNameHandler tableNameHandler;

    public MongoExecutor(MongoDatabase mongoDatabase, Set<String> mongoCollectionNames,TableNameHandler tableNameHandler){
        this.mongoDatabase = mongoDatabase;
        this.mongoCollectionNames = mongoCollectionNames;
        this.tableNameHandler = tableNameHandler;
    }

    @Override
    public Object intercept(Invocation invocation) throws Throwable {
        // 获取xml中的一个select/update/insert/delete节点，是一条SQL语句
        Object[] args = invocation.getArgs();
        MappedStatement mappedStatement = (MappedStatement) args[0];
        Object parameter = null;
        // 获取参数，if语句成立，表示sql语句有参数，参数格式是map形式
        if (invocation.getArgs().length > 1) {
            parameter = invocation.getArgs()[1];
        }
        // BoundSql就是封装myBatis最终产生的sql类
        BoundSql boundSql;
        if (args.length == 4) {
            boundSql = mappedStatement.getBoundSql(parameter);
        } else {
            // 几乎不可能走进这里面,除非使用Executor的代理对象调用query[args[6]]
            boundSql = (BoundSql) args[5];
        }
        List<String> tableNames = getTableNames(boundSql.getSql());
        if(isMongoCollection(tableNames)){
            // 获取节点的配置
            Configuration configuration = mappedStatement.getConfiguration();
            String sql = showSql(configuration,boundSql);
            // 获取到节点的id,即sql语句的id
            String id = mappedStatement.getId();
            SqlCommandType sqlType = mappedStatement.getSqlCommandType();
            if(tableNameHandler != null){
                sql = tableNameHandler.dynamicTableName(sql,tableNames);
            }
            log.info("mybatis拦截解析: \n ID: \t{}\n SQL: \t{}", id, sql);
            QueryConverter queryConverter = new QueryConverter.Builder().sqlString(sql).build();
            if(SqlCommandType.SELECT == sqlType){
                Class sqlClass = mappedStatement.getResultMaps().get(0).getType();
                return queryConverter.selectList(mongoDatabase, sqlClass);
            }else if(SqlCommandType.UPDATE == sqlType){
                return queryConverter.update(mongoDatabase);
            }else if(SqlCommandType.DELETE == sqlType){
                return queryConverter.delete(mongoDatabase);
            }else if(SqlCommandType.INSERT == sqlType){
                return queryConverter.insertList(mongoDatabase);
            }else {
                return null;
            }
        }else {
            return invocation.proceed();
        }
    }

    @Override
    public Object plugin(Object target) {
        return Plugin.wrap(target, this);
    }

    @Override
    public void setProperties(Properties properties) {
    }

    /**
     * 进行？的替换
     * @param configuration 配置类
     * @param boundSql 源sql信息
     * @return String 完整sql
     */
    public static String showSql(Configuration configuration, BoundSql boundSql) {
        // 获取参数
        Object parameterObject = boundSql.getParameterObject();
        List<ParameterMapping> parameterMappings = boundSql.getParameterMappings();
        // sql语句中多个空格都用一个空格代替
        String sql = boundSql.getSql().replaceAll("[\\s]+", " ");
        if (parameterMappings!= null && parameterMappings.size()>0 && parameterObject != null) {
            // 获取类型处理器注册器，类型处理器的功能是进行java类型和数据库类型的转换
            TypeHandlerRegistry typeHandlerRegistry = configuration.getTypeHandlerRegistry();
            // 如果根据parameterObject.getClass(）可以找到对应的类型，则替换
            if (typeHandlerRegistry.hasTypeHandler(parameterObject.getClass())) {
                sql = sql.replaceFirst("\\?",
                        Matcher.quoteReplacement(getParameterValue(parameterObject)));
            } else {
                // MetaObject主要是封装了originalObject对象，提供了get和set的方法用于获取和设置originalObject的属性值,主要支持对JavaBean、Collection、Map三种类型对象的操作
                MetaObject metaObject = configuration.newMetaObject(parameterObject);
                for (ParameterMapping parameterMapping : parameterMappings) {
                    String propertyName = parameterMapping.getProperty();
                    if (metaObject.hasGetter(propertyName)) {
                        Object obj = metaObject.getValue(propertyName);
                        sql = sql.replaceFirst("\\?",
                                Matcher.quoteReplacement(getParameterValue(obj)));
                    } else if (boundSql.hasAdditionalParameter(propertyName)) {
                        // 该分支是动态sql
                        Object obj = boundSql.getAdditionalParameter(propertyName);
                        sql = sql.replaceFirst("\\?",
                                Matcher.quoteReplacement(getParameterValue(obj)));
                    } else {
                        // 未知参数，替换？防止错位
                        sql = sql.replaceFirst("\\?", "unknown");
                    }
                }
            }
        }
        return sql;
    }

    /**
     * 如果参数是String，则添加单引号
     * 如果参数是日期，则转换为时间格式器并加单引号； 对参数是null和不是null的情况作了处理
     */
    private static String getParameterValue(Object obj) {
        String value;
        if (obj instanceof String) {
            value = "'" + obj.toString() + "'";
        } else if (obj instanceof Date) {
            DateFormat formatter = DateFormat.getDateTimeInstance(DateFormat.DEFAULT,
                    DateFormat.DEFAULT, Locale.CHINA);
            value = "'" + formatter.format(new Date()) + "'";
        } else {
            if (obj != null) {
                value = obj.toString();
            } else {
                value = "";
            }
        }
        return value;
    }

    protected boolean isMongoCollection(List<String> tableNames){
        for(String collectionName : tableNames){
            if(mongoCollectionNames.contains(collectionName)){
                return true;
            }
        }
        return false;
    }

    protected List<String> getTableNames(String sql) {
        List<String> tableNames = new ArrayList<>();
        TableNameParser parser = new TableNameParser(sql);
        List<TableNameParser.SqlToken> names = new ArrayList<>();
        parser.accept(names::add);
        for (TableNameParser.SqlToken name : names) {
            tableNames.add(name.getValue());
        }
        return tableNames;
    }

}
