package com.jerry.mybatis;

import java.util.List;

/**
 * @author lxw
 */
public interface TableNameHandler {

    /**
     * 生成动态表名
     * @param sql 原始sql
     * @param tableNames 多个表名
     * @return 替换后的sql
     */
    String dynamicTableName(String sql, List<String> tableNames);
}
