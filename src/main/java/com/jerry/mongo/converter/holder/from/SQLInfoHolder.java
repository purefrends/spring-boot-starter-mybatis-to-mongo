package com.jerry.mongo.converter.holder.from;

/**
 * Interface for classes that hold information.
 */
public interface SQLInfoHolder {

    /**
     * get the base table name.
     * @return the base table name
     */
    String getBaseTableName();

}
