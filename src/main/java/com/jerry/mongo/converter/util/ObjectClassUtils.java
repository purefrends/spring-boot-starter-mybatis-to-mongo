package com.jerry.mongo.converter.util;

import net.sf.jsqlparser.expression.*;

/**
 * @author jerry
 * 装换对象工具类
 */
public class ObjectClassUtils {

    public static boolean isBasicClass(Class o){
        if (o == Integer.class) {

        } else if (o == Long.class) {

        } else if (o == Short.class) {

        } else if (o == Boolean.class) {

        } else if (o == Byte.class) {

        } else if (o == Character.class) {

        } else if (o == Double.class) {

        } else if (o == Float.class) {

        } else if (o == String.class) {
        }else {
            return false;
        }
        return true;
    }

    public static Object objectToBasicClass(Expression expression){
        if (expression instanceof LongValue) {
            LongValue var = (LongValue)expression;
            return var.getValue();
        }else if (expression instanceof DoubleValue) {
            DoubleValue var = (DoubleValue)expression;
            return var.getValue();
        }else if (expression instanceof StringValue) {
            StringValue var = (StringValue)expression;
            return var.getValue();
        }else if(expression instanceof DateValue){
            DateValue var = (DateValue)expression;
            return var.getValue();
        }else {
           return null;
        }
    }

    public static <T> Object objectToDifBasicClass(Class<T> o, Object v) {
        String s = String.valueOf(v);
        if (o == Integer.class) {
            return Integer.valueOf(s);
        } else if (o == Long.class) {
            return Long.valueOf(s);
        } else if (o == Short.class) {
            return Short.valueOf(s);
        } else if (o == Boolean.class) {
            return Boolean.valueOf(s);
        } else if (o == Byte.class) {
            return Byte.valueOf(s);
        } else if (o == Double.class) {
            return Double.valueOf(s);
        } else if (o == Float.class) {
            return Float.valueOf(s);
        } else if (o == String.class) {
            return s;
        }else {
            return v;
        }
    }
}
