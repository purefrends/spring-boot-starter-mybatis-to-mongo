package com.jerry.mongo.converter;

/**
 * A enum which describes the type of SQL operation this is.
 */
public enum SQLCommandType {
    DELETE, SELECT, UPDATE,INSERT
}
